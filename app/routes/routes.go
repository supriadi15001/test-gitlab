package routes

import (
	"net/http"
	"test/handlers/user"
)

type ControllerList struct {
	UserController *user.UserController
}

func (c ControllerList) InitRoutes() *http.ServeMux {
	mux := http.NewServeMux()

	mux.HandleFunc("/signup", c.UserController.Register)

	return mux
}
