package main

import (
	"net/http"
	"os"
	"test/app/config"
	"test/app/routes"
	"test/bussinesses/user"

	"strconv"
	"test/driver/databases/mysql"
	_userDb "test/driver/databases/user"

	// "test/driver/mysql"
	_userController "test/handlers/user"
	"time"
)

func main() {
	config.LoadEnv()

	// connect to db
	configDB := mysql.ConfigDB{
		User:     os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
		Host:     os.Getenv("DB_HOST"),
		Port:     os.Getenv("DB_PORT"),
		Database: os.Getenv("DB_DATABASE"),
	}
	Conn := configDB.InitDB()

	timeoutContextEnv, _ := strconv.Atoi(os.Getenv("TIMEOUT_CONTEXT"))
	timeoutContext := time.Duration(timeoutContextEnv) * time.Second

	userUsecase := user.NewUseCase(_userDb.NewUserRepository(Conn), timeoutContext)

	userController := _userController.NewUserController(*userUsecase)

	// Routes
	routesInit := routes.ControllerList{
		UserController: userController,
	}
	mux := routesInit.InitRoutes()
	server := http.Server{
		Addr:    ":8080",
		Handler: mux,
	}
	err := server.ListenAndServe()
	if err != nil {
		panic(err)
	}
}
